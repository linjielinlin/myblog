---
title: gulp
date: 2016-10-01 00:00:00
tags: 
- JS
---
{% cq %} 与gulp相关的插件 {% endcq %}
<!--more-->
# gulp插件加载
>gulp-load-plugins

# js文档生成
>gulp-documentation

# 简化angular moudle声明
>gulp-ng-annotate

# 处理sass
>gulp-sass

# 生成sourceMap
>gulp-sourcemaps

# 实时更新
>browser-sync

# 雪碧图
>gulp.spritesmith