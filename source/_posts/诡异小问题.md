---
title: 诡异问题
date: 2016-11-03 11:11:11
tags: 
- 问题
---
{% cq %} 一些问题收集 {% endcq %}
<!--more-->
# 某些android版本(eg：5.0.1)iframe内部可以滚动主页面不能滚动的问题
```
<iframe src="url" frameborder="0" width="100%" scrolling="auto" onload="iframeHeight(this)"></iframe>
```
>iframe 里页页的内容将设有overflow的属性全部替换为eg:overflow-x: inherit;

# ie11 字体图标显示不出来(添加静态文件缓存即可）
```
# nginx config eg
 location ~* .(gif|jpeg|jpg|png|woff|ttf|otf|svg|woff2|eot|woff2\?*|eot\?*|ttf\?*)$ {
       proxy_pass   http://127.0.0.1:4673;
       proxy_set_header Host $host;
       proxy_set_header X-Forward-For $remote_addr;
       expires 1d;
       access_log /var/log/eot.log main;
       proxy_hide_header Pragma;
       add_header Cache-Control "public, max-age=86400";
   }
```
# ie css 不支持 unset initial 关键字
```
要设置unset前添加默认属性。
```
# Safari音视频不会自动播放，没有canplay监听 [详见](https://rcp.dyfchk2.kuxiao.cn/space/teacher-space.html#/coursesManager)
```
# 针对Safari preload="metadata"
if (navigator.userAgent.indexOf('Safari') > -1 && navigator.userAgent.indexOf('Chrome') < 1) {
                        O.canplay = true;
                    }else if (f.get('Player').readyState <= 2){
                        
                    }
```
# IOS 点击事件失效
```
<input type="checkbox" id=1>
<label for="1">选择</label>
```
```
<!-- 添加 onclick/touch事件 -->
<label onclick=""><input type="checkbox">选择</label>
```
```
<!-- 添加 css -->
label input {
  pointer-events: none;
}
```
# ie9只有前31个link或style标记关联的CSS能够应用，从第32个开始，其标记关联的CSS都将失效,CSS文件的大小不能超过288kb(超过切分为多个即可)。



